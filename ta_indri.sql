-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 09 Jul 2018 pada 17.57
-- Versi server: 10.1.33-MariaDB
-- Versi PHP: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ta_indri`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `barangs`
--

CREATE TABLE `barangs` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama_barang` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type_barang` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `merek_barang` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `satuan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `harga_satuan` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `barangs`
--

INSERT INTO `barangs` (`id`, `nama_barang`, `type_barang`, `merek_barang`, `satuan`, `harga_satuan`, `created_at`, `updated_at`) VALUES
(1, 'Double cylinder', '0810-10 US14', 'KEND', 'BUAH', 220000, '2018-07-09 07:58:55', '2018-07-09 07:58:55'),
(2, 'Double cylinder', '0810-13 US14', 'KEND', 'BUAH', 250000, '2018-07-09 07:59:21', '2018-07-09 07:59:21'),
(3, 'Lockcase Swing', 'K87735-25 US32D', 'KEND', 'BUAH', 390000, '2018-07-09 07:59:36', '2018-07-09 07:59:36'),
(4, 'Floor hinge', 'FH 84', 'KEND', 'BUAH', 2090000, '2018-07-09 07:59:57', '2018-07-09 07:59:57'),
(5, 'Gembok', '25452-03', 'KEND', 'BUAH', 180000, '2018-07-09 08:00:15', '2018-07-09 08:00:15'),
(6, 'ENGSEL', 'SEL0010 US32D', 'KEND', 'BUAH', 280000, '2018-07-09 08:00:43', '2018-07-09 08:00:43'),
(7, 'ENGSEL', 'SEL0007 US32D', 'KEND', 'BUAH', 180000, '2018-07-09 08:01:16', '2018-07-09 08:01:16'),
(8, 'Casement', 'CMT45 16\"', 'KEND', 'PASANG', 230000, '2018-07-09 08:01:56', '2018-07-09 08:01:56'),
(9, 'PATCH FITTING', 'PT 2210', 'KEND', 'BUAH', 360000, '2018-07-09 08:02:21', '2018-07-09 08:02:21'),
(10, 'PATCH FITTING', 'PT 2220', 'KEND', 'BUAH', 360000, '2018-07-09 08:02:43', '2018-07-09 08:02:43');

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_transaksi_pemesanans`
--

CREATE TABLE `detail_transaksi_pemesanans` (
  `id` int(10) UNSIGNED NOT NULL,
  `pesanan_id` int(10) UNSIGNED DEFAULT NULL,
  `barang_id` int(10) UNSIGNED DEFAULT NULL,
  `jumlah_beli` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `detail_transaksi_pemesanans`
--

INSERT INTO `detail_transaksi_pemesanans` (`id`, `pesanan_id`, `barang_id`, `jumlah_beli`, `total`, `created_at`, `updated_at`) VALUES
(35, 14, 2, 2, 500000, '2018-07-09 08:23:27', '2018-07-09 08:23:27'),
(36, 14, 3, 2, 780000, '2018-07-09 08:23:31', '2018-07-09 08:23:31'),
(37, 14, 5, 2, 360000, '2018-07-09 08:23:37', '2018-07-09 08:23:37'),
(38, 14, 7, 2, 360000, '2018-07-09 08:23:42', '2018-07-09 08:23:42'),
(39, 15, 5, 123, 22140000, '2018-07-09 08:23:55', '2018-07-09 08:23:55'),
(40, 15, 4, 100, 209000000, '2018-07-09 08:24:07', '2018-07-09 08:24:07'),
(41, 16, 7, 11, 1980000, '2018-07-09 08:24:21', '2018-07-09 08:24:21'),
(42, 16, 2, 11, 2750000, '2018-07-09 08:24:25', '2018-07-09 08:24:25'),
(43, 16, 3, 11, 4290000, '2018-07-09 08:24:32', '2018-07-09 08:24:32'),
(44, 16, 8, 112, 25760000, '2018-07-09 08:24:37', '2018-07-09 08:24:37'),
(45, 17, 4, 2, 4180000, '2018-07-09 08:24:50', '2018-07-09 08:24:50'),
(46, 18, 9, 10, 3600000, '2018-07-09 08:25:05', '2018-07-09 08:25:05'),
(47, 18, 10, 10, 3600000, '2018-07-09 08:25:11', '2018-07-09 08:25:11'),
(48, 18, 4, 10, 20900000, '2018-07-09 08:25:19', '2018-07-09 08:25:19'),
(49, 21, 4, 4, 8360000, '2018-07-09 08:25:43', '2018-07-09 08:25:43'),
(50, 26, 6, 1, 280000, '2018-07-09 08:26:07', '2018-07-09 08:26:07'),
(51, 26, 1, 2, 440000, '2018-07-09 08:26:12', '2018-07-09 08:26:12'),
(52, 26, 10, 4, 1440000, '2018-07-09 08:26:17', '2018-07-09 08:26:17'),
(53, 25, 6, 1, 280000, '2018-07-09 08:26:29', '2018-07-09 08:26:29'),
(54, 25, 5, 4, 720000, '2018-07-09 08:26:37', '2018-07-09 08:26:37'),
(55, 25, 9, 99, 35640000, '2018-07-09 08:26:44', '2018-07-09 08:26:44'),
(56, 24, 4, 10, 20900000, '2018-07-09 08:26:59', '2018-07-09 08:26:59'),
(57, 23, 1, 1, 220000, '2018-07-09 08:27:12', '2018-07-09 08:27:12'),
(58, 23, 8, 34, 7820000, '2018-07-09 08:27:19', '2018-07-09 08:27:19'),
(59, 23, 3, 11, 4290000, '2018-07-09 08:27:25', '2018-07-09 08:27:25'),
(60, 22, 3, 1, 390000, '2018-07-09 08:27:39', '2018-07-09 08:27:39'),
(61, 22, 8, 1000, 230000000, '2018-07-09 08:27:47', '2018-07-09 08:27:47'),
(62, 19, 6, 3, 840000, '2018-07-09 08:28:21', '2018-07-09 08:28:21'),
(63, 19, 1, 3, 660000, '2018-07-09 08:28:26', '2018-07-09 08:28:26'),
(64, 19, 9, 3, 1080000, '2018-07-09 08:28:31', '2018-07-09 08:28:31'),
(65, 19, 4, 3, 6270000, '2018-07-09 08:28:38', '2018-07-09 08:28:38'),
(66, 19, 5, 3, 540000, '2018-07-09 08:28:51', '2018-07-09 08:28:51'),
(67, 20, 7, 1890, 340200000, '2018-07-09 08:29:06', '2018-07-09 08:29:06'),
(68, 27, 3, 1, 390000, '2018-07-09 08:31:55', '2018-07-09 08:31:55'),
(69, 28, 7, 23, 4140000, '2018-07-09 08:32:16', '2018-07-09 08:32:16');

-- --------------------------------------------------------

--
-- Struktur dari tabel `hasil_clusterings`
--

CREATE TABLE `hasil_clusterings` (
  `id` int(10) UNSIGNED NOT NULL,
  `pelanggan_id` int(10) UNSIGNED DEFAULT NULL,
  `kelompok` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rekomendasi` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `jaraks`
--

CREATE TABLE `jaraks` (
  `id` int(10) UNSIGNED NOT NULL,
  `pelanggan_id` int(10) UNSIGNED DEFAULT NULL,
  `transformasi_id` int(10) UNSIGNED DEFAULT NULL,
  `periode_clustering` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kelompok_1` double(8,3) NOT NULL,
  `kelompok_2` double(8,3) NOT NULL,
  `kelompok_3` double(8,3) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `jaraks`
--

INSERT INTO `jaraks` (`id`, `pelanggan_id`, `transformasi_id`, `periode_clustering`, `kelompok_1`, `kelompok_2`, `kelompok_3`, `created_at`, `updated_at`) VALUES
(14, 1, 14, '07.2018', 3.162, 5.000, 1.000, '2018-07-09 08:32:29', '2018-07-09 08:32:29'),
(15, 2, 15, '07.2018', 4.123, 6.481, 1.414, '2018-07-09 08:32:29', '2018-07-09 08:32:29'),
(16, 4, 16, '07.2018', 3.317, 5.477, 2.828, '2018-07-09 08:32:29', '2018-07-09 08:32:29'),
(17, 5, 17, '07.2018', 3.317, 5.831, 0.000, '2018-07-09 08:32:29', '2018-07-09 08:32:29'),
(18, 7, 18, '07.2018', 4.123, 6.481, 1.414, '2018-07-09 08:32:29', '2018-07-09 08:32:29'),
(19, 8, 19, '07.2018', 3.317, 5.477, 2.828, '2018-07-09 08:32:29', '2018-07-09 08:32:29');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_07_04_151333_create_barangs_table', 1),
(4, '2018_07_04_151350_create_pelanggans_table', 1),
(5, '2018_07_05_132605_create_transaksi_pemesanans_table', 1),
(6, '2018_07_05_132625_create_detail_transaksi_pemesanans_table', 1),
(7, '2018_07_08_122846_create_transformasis_table', 1),
(8, '2018_07_08_145152_create_jaraks_table', 1),
(9, '2018_07_09_125745_create_hasil_clusterings_table', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pelanggans`
--

CREATE TABLE `pelanggans` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama_pelanggan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_telp` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `diskon` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `pelanggans`
--

INSERT INTO `pelanggans` (`id`, `nama_pelanggan`, `no_telp`, `email`, `alamat`, `diskon`, `created_at`, `updated_at`) VALUES
(1, 'PT ALAM INDAH GRAHA UTAMA', '031-9867867', 'alamindah@graha.com', 'Surabaya', 5, '2018-07-09 07:51:59', '2018-07-09 07:51:59'),
(2, 'PT. ALWINDO NUSANTARA', '08766667766676', 'alwindo@gmail.com', 'surabaya', 5, '2018-07-09 07:52:43', '2018-07-09 07:52:43'),
(3, 'PT. CIPTA KARSA BUMI LESTARI', '0898787738667', 'ciptakarsa@gmail.com', 'surabaya', 5, '2018-07-09 07:53:36', '2018-07-09 07:53:36'),
(4, 'PT. HAMPARAN INDAH MANDIRI', '08347776767652', 'hamparanindah@gmail.com', 'sidoarjo', 5, '2018-07-09 07:54:17', '2018-07-09 07:54:17'),
(5, 'PT. INDO CITRA EKA ABADI', '031-978673', 'icea@gmail.com', 'surabaya', 5, '2018-07-09 07:55:07', '2018-07-09 07:57:05'),
(6, 'PT. SAMATOR LAND', '082345678934', 'samator@gmail.com', 'surabaya', 5, '2018-07-09 07:56:52', '2018-07-09 07:56:52'),
(7, 'TOKO CENDRAWASIH', '031-8746476', 'cendra@gmail.com', 'surabaya', 5, '2018-07-09 07:57:49', '2018-07-09 07:57:49'),
(8, 'PT. WELLGAN', '0876647646475', 'wellgan@gmail.com', 'semarang', 5, '2018-07-09 07:58:36', '2018-07-09 07:58:36');

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi_pemesanans`
--

CREATE TABLE `transaksi_pemesanans` (
  `id` int(10) UNSIGNED NOT NULL,
  `pelanggan_id` int(10) UNSIGNED DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `transaksi_pemesanans`
--

INSERT INTO `transaksi_pemesanans` (`id`, `pelanggan_id`, `total`, `status`, `created_at`, `updated_at`) VALUES
(14, 1, 1900000, 1, '2018-07-09 08:21:37', '2018-07-09 08:23:45'),
(15, 4, 219583000, 1, '2018-07-09 08:21:43', '2018-07-09 08:24:10'),
(16, 1, 33041000, 1, '2018-07-09 08:21:48', '2018-07-09 08:24:40'),
(17, 5, 3971000, 1, '2018-07-09 08:21:54', '2018-07-09 08:24:53'),
(18, 8, 26695000, 1, '2018-07-09 08:22:01', '2018-07-09 08:25:22'),
(19, 7, 8920500, 1, '2018-07-09 08:22:35', '2018-07-09 08:28:54'),
(20, 8, 323190000, 1, '2018-07-09 08:22:41', '2018-07-09 08:29:10'),
(21, 2, 7942000, 1, '2018-07-09 08:22:46', '2018-07-09 08:25:47'),
(22, 4, 218870500, 1, '2018-07-09 08:22:51', '2018-07-09 08:27:50'),
(23, 5, 11713500, 1, '2018-07-09 08:22:58', '2018-07-09 08:27:28'),
(24, 7, 19855000, 1, '2018-07-09 08:23:02', '2018-07-09 08:27:02'),
(25, 5, 34808000, 1, '2018-07-09 08:23:14', '2018-07-09 08:26:47'),
(26, 8, 2052000, 1, '2018-07-09 08:23:19', '2018-07-09 08:26:19'),
(27, 1, 370500, 1, '2018-07-09 08:31:46', '2018-07-09 08:31:58'),
(28, 1, 3933000, 1, '2018-07-09 08:32:03', '2018-07-09 08:32:19');

-- --------------------------------------------------------

--
-- Struktur dari tabel `transformasis`
--

CREATE TABLE `transformasis` (
  `id` int(10) UNSIGNED NOT NULL,
  `pelanggan_id` int(10) UNSIGNED DEFAULT NULL,
  `periode_clustering` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `trans_total` int(11) NOT NULL,
  `trans_frekuensi` int(11) NOT NULL,
  `trans_barang` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `transformasis`
--

INSERT INTO `transformasis` (`id`, `pelanggan_id`, `periode_clustering`, `trans_total`, `trans_frekuensi`, `trans_barang`, `created_at`, `updated_at`) VALUES
(14, 1, '07.2018', 2, 2, 2, '2018-07-09 08:32:28', '2018-07-09 08:32:28'),
(15, 2, '07.2018', 1, 1, 1, '2018-07-09 08:32:28', '2018-07-09 08:32:28'),
(16, 4, '07.2018', 4, 1, 4, '2018-07-09 08:32:28', '2018-07-09 08:32:28'),
(17, 5, '07.2018', 2, 1, 2, '2018-07-09 08:32:28', '2018-07-09 08:32:28'),
(18, 7, '07.2018', 1, 1, 1, '2018-07-09 08:32:28', '2018-07-09 08:32:28'),
(19, 8, '07.2018', 4, 1, 4, '2018-07-09 08:32:29', '2018-07-09 08:32:29');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `level`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Indri', 'me@admin.com', '$2y$10$.0Sm8wawqN.tA9R5mw6gPO5PSjUC7iC90uOJJcq0Xy6Q4DGwrUlmq', 'admin', NULL, '2018-07-09 07:45:17', '2018-07-09 07:45:17');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `barangs`
--
ALTER TABLE `barangs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `detail_transaksi_pemesanans`
--
ALTER TABLE `detail_transaksi_pemesanans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `detail_transaksi_pemesanans_pesanan_id_foreign` (`pesanan_id`),
  ADD KEY `detail_transaksi_pemesanans_barang_id_foreign` (`barang_id`);

--
-- Indeks untuk tabel `hasil_clusterings`
--
ALTER TABLE `hasil_clusterings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `hasil_clusterings_pelanggan_id_foreign` (`pelanggan_id`);

--
-- Indeks untuk tabel `jaraks`
--
ALTER TABLE `jaraks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jaraks_pelanggan_id_foreign` (`pelanggan_id`),
  ADD KEY `jaraks_transformasi_id_foreign` (`transformasi_id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `pelanggans`
--
ALTER TABLE `pelanggans`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `transaksi_pemesanans`
--
ALTER TABLE `transaksi_pemesanans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transaksi_pemesanans_pelanggan_id_foreign` (`pelanggan_id`);

--
-- Indeks untuk tabel `transformasis`
--
ALTER TABLE `transformasis`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transformasis_pelanggan_id_foreign` (`pelanggan_id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `barangs`
--
ALTER TABLE `barangs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `detail_transaksi_pemesanans`
--
ALTER TABLE `detail_transaksi_pemesanans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT untuk tabel `hasil_clusterings`
--
ALTER TABLE `hasil_clusterings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `jaraks`
--
ALTER TABLE `jaraks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `pelanggans`
--
ALTER TABLE `pelanggans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `transaksi_pemesanans`
--
ALTER TABLE `transaksi_pemesanans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT untuk tabel `transformasis`
--
ALTER TABLE `transformasis`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `detail_transaksi_pemesanans`
--
ALTER TABLE `detail_transaksi_pemesanans`
  ADD CONSTRAINT `detail_transaksi_pemesanans_barang_id_foreign` FOREIGN KEY (`barang_id`) REFERENCES `barangs` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `detail_transaksi_pemesanans_pesanan_id_foreign` FOREIGN KEY (`pesanan_id`) REFERENCES `transaksi_pemesanans` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `hasil_clusterings`
--
ALTER TABLE `hasil_clusterings`
  ADD CONSTRAINT `hasil_clusterings_pelanggan_id_foreign` FOREIGN KEY (`pelanggan_id`) REFERENCES `pelanggans` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `jaraks`
--
ALTER TABLE `jaraks`
  ADD CONSTRAINT `jaraks_pelanggan_id_foreign` FOREIGN KEY (`pelanggan_id`) REFERENCES `pelanggans` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `jaraks_transformasi_id_foreign` FOREIGN KEY (`transformasi_id`) REFERENCES `transformasis` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `transaksi_pemesanans`
--
ALTER TABLE `transaksi_pemesanans`
  ADD CONSTRAINT `transaksi_pemesanans_pelanggan_id_foreign` FOREIGN KEY (`pelanggan_id`) REFERENCES `pelanggans` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `transformasis`
--
ALTER TABLE `transformasis`
  ADD CONSTRAINT `transformasis_pelanggan_id_foreign` FOREIGN KEY (`pelanggan_id`) REFERENCES `pelanggans` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
