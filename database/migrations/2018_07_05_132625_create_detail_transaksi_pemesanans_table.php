<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailTransaksiPemesanansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_transaksi_pemesanans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pesanan_id')->unsigned()->nullable();
            $table->foreign('pesanan_id')->references('id')->on('transaksi_pemesanans')->onDelete('cascade');
            $table->integer('barang_id')->unsigned()->nullable();
            $table->foreign('barang_id')->references('id')->on('barangs')->onDelete('cascade');
            $table->integer('jumlah_beli');
            $table->integer('total');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_transaksi_pemesanans');
    }
}
