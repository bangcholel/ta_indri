<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJaraksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jaraks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pelanggan_id')->unsigned()->nullable();
            $table->foreign('pelanggan_id')->references('id')->on('pelanggans')->onDelete('cascade');
            $table->integer('transformasi_id')->unsigned()->nullable();
            $table->foreign('transformasi_id')->references('id')->on('transformasis')->onDelete('cascade');
            $table->string('periode_clustering');
            $table->float('kelompok_1', 8, 3);
            $table->float('kelompok_2', 8, 3);
            $table->float('kelompok_3', 8, 3);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jaraks');
    }
}
