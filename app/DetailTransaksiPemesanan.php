<?php

namespace App;

use App\Barang;
use App\TransaksiPemesanan;
use Illuminate\Database\Eloquent\Model;

class DetailTransaksiPemesanan extends Model
{
    protected $fillable = ['pesanan_id', 'barang_id', 'jumlah_beli', 'total'];

    public function transaksi() {
        return $this->belongsTo(TransaksiPemesanan::class, 'pesanan_id');
    }

    public function barang() {
        return $this->belongsTo(Barang::class);
    }
}
