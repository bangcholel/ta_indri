<?php

namespace App;

use App\DetailTransaksiPemesanan;
use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    protected $fillable = ['nama_barang', 'type_barang', 'merek_barang', 'satuan', 'harga_satuan'];

    public function detilTransaksi() {
        return $this->hasMany(DetailTransaksiPemesanan::class);
    }
}
