<?php

namespace App;

use App\Pelanggan;
use App\DetailTransaksiPemesanan;
use Illuminate\Database\Eloquent\Model;

class TransaksiPemesanan extends Model
{
    protected $fillable = ['pelanggan_id', 'total', 'status'];

    public function pelanggan() {
        return $this->belongsTo(Pelanggan::class);
    }

    public function detilTransaksi() {
        return $this->hasMany(DetailTransaksiPemesanan::class, 'pesanan_id');
    }
}
