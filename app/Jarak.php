<?php

namespace App;

use App\Pelanggan;
use App\Transformasi;
use Illuminate\Database\Eloquent\Model;

class Jarak extends Model
{
    protected $fillable = ['pelanggan_id', 'transformasi_id', 'periode_clustering', 'kelompok_1', 'kelompok_2', 'kelompok_3'];

    public function pelanggan() {
        return $this->belongsTo(Pelanggan::class);
    }

    public function transformasi() {
        return $this->belongsTo(Transformasi::class);
    }
}
