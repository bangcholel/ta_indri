<?php

namespace App;

use App\Jarak;
use App\Transformasi;
use App\HasilClustering;
use App\TransaksiPemesanan;
use Illuminate\Database\Eloquent\Model;

class Pelanggan extends Model
{
    protected $fillable = ['nama_pelanggan', 'no_telp', 'email', 'alamat', 'diskon'];
    public function transaksiPemesanans() {
        return $this->hasMany(TransaksiPemesanan::class);
    }

    public function transformasi() {
        return $this->hasMany(Transformasi::class);
    }

    public function jarak() {
        return $this->hasMany(Jarak::class);
    }

    public function hasilClustering() {
        return $this->hasOne(HasilClustering::class);
    }
}
