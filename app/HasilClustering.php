<?php

namespace App;

use App\Pelanggan;
use Illuminate\Database\Eloquent\Model;

class HasilClustering extends Model
{
    protected $fillable = ['pelanggan_id', 'kelompok', 'rekomendasi'];

    public function pelanggan() {
        return $this->belongsTo(Pelanggan::class);
    }
}
