<?php

namespace App;

use App\Jarak;
use App\Pelanggan;
use Illuminate\Database\Eloquent\Model;

class Transformasi extends Model
{
    protected $fillable = ['pelanggan_id', 'periode_clustering', 'trans_total', 'trans_frekuensi', 'trans_barang'];

    public function pelanggan() {
        return $this->belongsTo(Pelanggan::class);
    }

    public function jarak() {
        return $this->hasOne(Jarak::class);
    }
}
