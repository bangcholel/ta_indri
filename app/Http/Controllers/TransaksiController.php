<?php

namespace App\Http\Controllers;

use Session;
use App\Barang;
use App\Pelanggan;
use App\TransaksiPemesanan;
use Illuminate\Http\Request;
use App\DetailTransaksiPemesanan;

class TransaksiController extends Controller
{
    public function index() {
        $transaksi = TransaksiPemesanan::all();
        $pelanggan = Pelanggan::all();
        return view('transaksi.index')
            ->with('transaksi', $transaksi)
            ->with('pelanggan', $pelanggan);
    }

    public function store(Request $request) {
        $this->validate($request, [
            'pelanggan_id' => 'required'
        ]);

        TransaksiPemesanan::create([
            'pelanggan_id' => $request->pelanggan_id,
            'status' => 0
        ]);

        Session::flash('success', 'Berhasil menambah transaksi pemsanan');
        return redirect()->back();
    }

    public function show($transaksi) {
        $t = TransaksiPemesanan::find($transaksi);
        $barang = Barang::all();
        $detil = $t->detilTransaksi;
        return view('transaksi.show')
            ->with('transaksi', $t)
            ->with('barang', $barang)
            ->with('detil', $detil);
    }

    public function update(Request $request, $transaksi) {
        $this->validate($request, [
            'barang_id' => 'required',
            'jumlah_beli' => 'required'
        ]);

        $t = TransaksiPemesanan::find($transaksi);

        $barang = Barang::find($request->barang_id);
        $hitungTotal = $request->jumlah_beli * $barang->harga_satuan;
        $d = DetailTransaksiPemesanan::create([
            'pesanan_id' => $t->id,
            'barang_id' => $request->barang_id,
            'jumlah_beli' => $request->jumlah_beli,
            'total' => $hitungTotal
        ]);
        if ($t->total == null) {
            $t->total = $hitungTotal;
            $t->save();
        } else {
            $t->total += $hitungTotal;
            $t->save();
        }
        Session::flash('success', 'Berhasil menambah detil transaksi');
        return redirect()->back();
    }

    public function destroy($transaksi) {
        $t = TransaksiPemesanan::find($transaksi);
        $t->delete();
        Session::flash('success', 'Berhasil menghapus transaksi');
        return redirect()->back();
    }

    public function fix(Request $request, $id) {
        $transaksi = TransaksiPemesanan::find($id);
        $transaksi->status = 1;
        $diskon = $request->total * $transaksi->pelanggan->diskon / 100;
        $transaksi->total = $transaksi->total - $diskon;
        $transaksi->save();
        Session::flash('success', 'Transaksi Disimpan');
        return redirect()->route('transaksi.index');
    }
}
