<?php

namespace App\Http\Controllers;

use Session;
use App\Pelanggan;
use Illuminate\Http\Request;

class PelangganController extends Controller
{
    public function index() {
        $pelanggans = Pelanggan::all();
        return view('pelanggan.index')->with('pelanggans', $pelanggans);
    }

    public function create() {
        return view('pelanggan.create');
    }

    public function store(Request $request) {
        $this->validate($request, [
            'nama_pelanggan' => 'required',
            'no_telp' => 'required',
            'email' => 'required',
            'alamat' => 'required',
        ]);

        Pelanggan::create([
            'nama_pelanggan' => $request->nama_pelanggan,
            'no_telp' => $request->no_telp,
            'email' =>  $request->email,
            'alamat' => $request->alamat,
            'diskon' => 5
        ]);

        Session::flash('success', 'Berhasil nama pelanggan');
        return redirect()->route('pelanggan.index');
    }

    public function edit($pelanggan) {
        $p = Pelanggan::find($pelanggan);
        return view('pelanggan.edit')->with('pelanggan', $p);
    }

    public function update(Request $request, $pelanggan) {
        $this->validate($request, [
            'nama_pelanggan' => 'required',
            'no_telp' => 'required',
            'email' => 'required',
            'alamat' => 'required',
        ]);

        $p = Pelanggan::find($pelanggan);
        $p->update([
            'nama_pelanggan' => $request->nama_pelanggan,
            'no_telp' => $request->no_telp,
            'email' => $request->email,
            'alamat' => $request->alamat,
            'diskon' => 5
        ]);

        Session::flash('success', 'Berhasil memperbarui pelanggan');
        return redirect()->route('pelanggan.index');
    }

    public function destroy($pelanggan) {
        $p = Pelanggan::find($pelanggan);
        $p->delete();
        Session::flash('success', 'Berhasil menghapus pelanggan');
        return redirect()->back();
    }
}
