<?php

namespace App\Http\Controllers;

use Session;
use App\Barang;
use Illuminate\Http\Request;

class BarangController extends Controller
{
    public function index() {
        $barangs = Barang::all();
        return view('barang.index')->with('barangs', $barangs);
    }

    public function create() {
        return view('barang.create');
    }

    public function store(Request $request) {
        $this->validate($request, [
            'nama_barang' => 'required',
            'type_barang' => 'required',
            'merek_barang' => 'required',
            'satuan' => 'required',
            'harga_satuan' => 'required'
        ]);

        Barang::create([
            'nama_barang' => $request->nama_barang,
            'type_barang' => $request->type_barang,
            'merek_barang' => $request->merek_barang,
            'satuan' => $request->satuan,
            'harga_satuan' => $request->harga_satuan
        ]);

        Session::flash('success', 'Berhasil menambahkan barang');
        return redirect()->route('barang.index');
    }

    public function edit($barang) {
        $b = Barang::find($barang);
        return view('barang.edit')->with('barang', $b);
    }

    public function update(Request $request, $barang) {
        $b = Barang::find($barang);
        $this->validate($request, [
            'nama_barang' => 'required',
            'type_barang' => 'required',
            'merek_barang' => 'required',
            'satuan' => 'required',
            'harga_satuan' => 'required'
        ]);

        $b->update([
            'nama_barang' => $request->nama_barang,
            'type_barang' => $request->type_barang,
            'merek_barang' => $request->merek_barang,
            'satuan' => $request->satuan,
            'harga_satuan' => $request->harga_satuan
        ]);

        Session::flash('success', 'Berhasil mengupdate barang');
        return redirect()->route('barang.index');
    }

    public function destroy($barang) {
        $b = Barang::find($barang);
        $b->delete();
        Session::flash('success', 'Berhasil menghapus barang');
        return redirect()->back();
    }
}
