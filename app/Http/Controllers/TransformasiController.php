<?php

namespace App\Http\Controllers;

use Session;
use App\Jarak;
use App\Transformasi;
use App\TransaksiPemesanan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TransformasiController extends Controller
{
    public function index() {
        $transformasi = Transformasi::all();
        return view('transformasi.index')->with('transformasi', $transformasi);
    }

    public function store(Request $request) {
        $this->validate($request, [
            'periode_clustering' => 'required'
        ]);

        $audit = DB::table('transaksi_pemesanans')
                ->join('pelanggans', 'transaksi_pemesanans.pelanggan_id', '=', 'pelanggans.id')
                ->select('pelanggan_id', 'pelanggans.nama_pelanggan')
                ->whereMonth('transaksi_pemesanans.created_at', '=', $request->periode_clustering)
                ->distinct()->get();

        $hasil = [];
        foreach ($audit as $a) {
            $tran = TransaksiPemesanan::where('pelanggan_id', '=', $a->pelanggan_id)->get();
            $frek = count($tran);
            $grand_total = 0;
            $total_qty = 0;
            foreach($tran as $t) {
                $grand_total += $t->total;
                foreach($t->detilTransaksi as $detil) {
                    $total_qty += $detil->jumlah_beli;
                }
            }
            array_push($hasil, ['bulan' => $request->periode_clustering.'.'.date('Y'), 'pelanggan_id' => $a->pelanggan_id, 'nama_pelanggan' => $a->nama_pelanggan, 'grand_total' => $grand_total, 'qty_total' => $total_qty, 'frekuensi' => $frek]);
        }

        $totalTrans = 0;
        $frekTrans = 0;
        $qtyTrans = 0;
        $hasilTrans = [];

        for ($i = 0; $i < count($hasil); $i++) {
            if ($hasil[$i]['grand_total'] > 0 && $hasil[$i]['grand_total'] <= 30000000) {
                $totalTrans = 1;
            } else if ($hasil[$i]['grand_total'] > 30000000 && $hasil[$i]['grand_total'] <= 60000000) {
                $totalTrans = 2;
            } else if ($hasil[$i]['grand_total'] > 60000000 && $hasil[$i]['grand_total'] <= 90000000) {
                $totalTrans = 3;
            } else if ($hasil[$i]['grand_total'] > 90000000) {
                $totalTrans = 4;
            }

            if ($hasil[$i]['frekuensi'] > 0 && $hasil[$i]['frekuensi'] <= 3) {
                $frekTrans = 1;
            } else if ($hasil[$i]['frekuensi'] > 3 && $hasil[$i]['frekuensi'] <= 6) {
                $frekTrans = 2;
            } else if ($hasil[$i]['frekuensi'] > 6 && $hasil[$i]['frekuensi'] <= 9) {
                $frekTrans = 3;
            } else if ($hasil[$i]['frekuensi'] > 9) {
                $frekTrans = 4;
            }

            if ($hasil[$i]['qty_total'] > 0 && $hasil[$i]['qty_total'] <= 150) {
                $qtyTrans = 1;
            } else if ($hasil[$i]['qty_total'] > 150 && $hasil[$i]['qty_total'] <= 300) {
                $qtyTrans = 2;
            } else if ($hasil[$i]['qty_total'] > 300 && $hasil[$i]['qty_total'] <= 450) {
                $qtyTrans = 3;
            } else if ($hasil[$i]['qty_total'] > 450) {
                $qtyTrans = 4;
            }
            Transformasi::create([
                'pelanggan_id' => $hasil[$i]['pelanggan_id'],
                'periode_clustering' => $hasil[$i]['bulan'],
                'trans_total' => $totalTrans,
                'trans_frekuensi' => $frekTrans,
                'trans_barang' => $qtyTrans
            ]);
        }

        $transformasi = Transformasi::where('periode_clustering', '=', $request->periode_clustering.'.'.date('Y'))->get();
        foreach ($transformasi as $trans) { 
            $kelompok1 = sqrt( (($trans->trans_total - 5) * ($trans->trans_total - 5)) + (($trans->trans_frekuensi - 2) * ($trans->trans_frekuensi - 2)) + (($trans->trans_barang - 1) * ($trans->trans_barang - 1)) );
            $kelompok2 = sqrt( (($trans->trans_total - 5) * ($trans->trans_total - 5)) + (($trans->trans_frekuensi - 6) * ($trans->trans_frekuensi - 6)) + (($trans->trans_barang - 2) * ($trans->trans_barang - 2)) );
            $kelompok3 = sqrt( (($trans->trans_total - 2) * ($trans->trans_total - 2)) + (($trans->trans_frekuensi - 1) * ($trans->trans_frekuensi - 1)) + (($trans->trans_barang - 2) * ($trans->trans_barang - 2)) );
            $kelompok1 = sprintf("%0.3f", round($kelompok1, 3));
            $kelompok2 = sprintf("%0.3f", round($kelompok2, 3));
            $kelompok3 = sprintf("%0.3f", round($kelompok3, 3));
            Jarak::create([
                'pelanggan_id' => $trans->pelanggan_id,
                'transformasi_id' => $trans->id,
                'periode_clustering' => $trans->periode_clustering,
                'kelompok_1' => $kelompok1,
                'kelompok_2' => $kelompok2,
                'kelompok_3' => $kelompok3
            ]);
        }
        Session::flash('success', 'berhasil membuat transformasi');
        return redirect()->back();
    }
}
