<?php

namespace App\Http\Controllers;

use App\Jarak;
use App\Pelanggan;
use App\Transformasi;
use App\HasilClustering;
use App\TransaksiPemesanan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TestController extends Controller
{
    public function test() {
        // return response()->json(ini_get('max_execution_time'));
        
        $urutan = "";
        $hasilUrutan = "";
        
        
        $hasilFix = [];
        $test = 123;
        while($test > 0) {
            $hasilUrutan = "";
            $urutan = "";
            $test = '';
            $jarak = '';
            $jarak = Jarak::where('periode_clustering', '=', '07.2018')->get();
            $data = [];
            foreach($jarak as $j) {
                if ($j->kelompok_1 <= $j->kelompok_2 && $j->kelompok_1 <= $j->kelompok_3) {
                    array_push($data, ['data' => 1, 'transformasi_id' => $j->transformasi_id, 'pelanggan_id' => $j->pelanggan_id, 'jarak_id' => $j->id, 'kelompok1' => $j->kelompok_1, 'kelompok2' => $j->kelompok_2, 'kelompok3' => $j->kelompok_3]);
                    $urutan = $urutan . "1";
                } else if ($j->kelompok_2 <= $j->kelompok_1 && $j->kelompok_2 <= $j->kelompok_3) {
                    array_push($data, ['data' => 2, 'transformasi_id' => $j->transformasi_id, 'pelanggan_id' => $j->pelanggan_id, 'jarak_id' => $j->id, 'kelompok1' => $j->kelompok_1, 'kelompok2' => $j->kelompok_2, 'kelompok3' => $j->kelompok_3]);
                    $urutan = $urutan . "2";
                } else if ($j->kelompok_3 <= $j->kelompok_1 && $j->kelompok_3 <= $j->kelompok_2) {
                    array_push($data, ['data' => 3, 'transformasi_id' => $j->transformasi_id, 'pelanggan_id' => $j->pelanggan_id, 'jarak_id' => $j->id, 'kelompok1' => $j->kelompok_1, 'kelompok2' => $j->kelompok_2, 'kelompok3' => $j->kelompok_3]);
                    $urutan = $urutan . "3";
                }
            }

            // return response()->json($data);
            $transformasi = Transformasi::where('periode_clustering', '=', '07.2018')->get();
            $qty = 0;
            $frek = 0;
            $tot = 0;
            $zzz = [];
            $totalFrek = 0;
            $totalQty = 0;
            $totalTot = 0;

            $centroid1_tot = 0;
            $centroid1_frek = 0;
            $centroid1_bar = 0;

            $centroid2_tot = 0;
            $centroid2_frek = 0;
            $centroid2_bar = 0;

            $centroid3_tot = 0;
            $centroid3_frek = 0;
            $centroid3_bar = 0;
            for($i = 0; $i < count($transformasi); $i++) {
                if ($data[$i]['data'] == 1) {
                    $t = Transformasi::find($data[$i]['transformasi_id']);
                    $centroid1_tot += $t->trans_total;
                    $centroid1_frek += $t->trans_frekuensi;
                    $centroid1_bar += $t->trans_barang;
                    $totalTot++;
                } else if ($data[$i]['data'] == 2) {
                    $t = Transformasi::find($data[$i]['transformasi_id']);
                    $centroid2_tot += $t->trans_total;
                    $centroid2_frek += $t->trans_frekuensi;
                    $centroid2_bar += $t->trans_barang;
                    $totalFrek++;
                } else if ($data[$i]['data'] == 3) {
                    $t = Transformasi::find($data[$i]['transformasi_id']);
                    $centroid3_tot += $t->trans_total;
                    $centroid3_frek += $t->trans_frekuensi;
                    $centroid3_bar += $t->trans_barang;
                    $totalQty++;
                }
            }

            $finalCen1_tot = 0;
            $finalCen1_frek = 0;
            $finalCen1_bar = 0;

            $finalCen2_tot = 0;
            $finalCen2_frek = 0;
            $finalCen2_bar = 0;

            $finalCen3_tot = 0;
            $finalCen3_frek = 0;
            $finalCen3_bar = 0;

            if ($totalTot != 0) {
                $finalCen1_tot = $centroid1_tot / $totalTot;
                $finalCen1_frek = $centroid1_frek / $totalTot;
                $finalCen1_bar = $centroid1_bar / $totalTot;
            } else {
                $finalCen1_tot = 0;
                $finalCen1_frek = 0;
                $finalCen1_bar = 0;
            }

            if ($totalFrek != 0) {
                $finalCen2_tot = $centroid2_tot / $totalFrek;
                $finalCen2_frek = $centroid2_frek / $totalFrek;
                $finalCen2_bar = $centroid2_bar / $totalFrek;
            } else {
                $finalCen2_tot = 0;
                $finalCen2_frek = 0;
                $finalCen2_bar = 0;
            }

            if ($totalQty != 0) {
                $finalCen3_tot = $centroid3_tot / $totalQty;
                $finalCen3_frek = $centroid3_frek / $totalQty;
                $finalCen3_bar = $centroid3_bar / $totalQty;
            } else {
                $finalCen3_tot = 0;
                $finalCen3_frek = 0;
                $finalCen3_bar = 0;
            }

            $hasilAkhir = [];
            $transformasi = Transformasi::where('periode_clustering', '=', '07.2018')->get();
            $loop = 0;
            foreach ($transformasi as $trans) { 
                $kelompok1 = sqrt( (($trans->trans_total - $finalCen1_tot) * ($trans->trans_total - $finalCen1_tot)) + (($trans->trans_frekuensi - $finalCen1_frek) * ($trans->trans_frekuensi - $finalCen1_frek)) + (($trans->trans_barang - $finalCen1_bar) * ($trans->trans_barang - $finalCen1_bar)) );
                $kelompok2 = sqrt( (($trans->trans_total - $finalCen2_tot) * ($trans->trans_total - $finalCen2_tot)) + (($trans->trans_frekuensi - $finalCen2_frek) * ($trans->trans_frekuensi - $finalCen2_frek)) + (($trans->trans_barang - $finalCen2_tot) * ($trans->trans_barang - $finalCen2_tot)) );
                $kelompok3 = sqrt( (($trans->trans_total - $finalCen3_tot) * ($trans->trans_total - $finalCen3_tot)) + (($trans->trans_frekuensi - $finalCen3_frek) * ($trans->trans_frekuensi - $finalCen3_frek)) + (($trans->trans_barang - $finalCen3_bar) * ($trans->trans_barang - $finalCen3_bar)) );
                $kelompok1 = sprintf("%0.3f", round($kelompok1, 3));
                $kelompok2 = sprintf("%0.3f", round($kelompok2, 3));
                $kelompok3 = sprintf("%0.3f", round($kelompok3, 3));

                $jarak = Jarak::find($data[$loop]['jarak_id']);
                $jarak->kelompok_1 = $kelompok1;
                $jarak->kelompok_2 = $kelompok2;
                $jarak->kelompok_3 = $kelompok3;
                $jarak->save();
                array_push($hasilAkhir, ['kelompok1' => $kelompok1, 'kelompok2' => $kelompok2, 'kelompok3' => $kelompok3, 'pelanggan_id' => $data[$loop]['pelanggan_id'], 'jarak_id' => $data[$loop]['jarak_id']]);
                $loop++;
            }

            // $hasilFix = [];
            unset($hasilFix); // $hasilFix is gone
            $hasilFix = [];
            for ($i=0; $i < count($hasilAkhir); $i++) { 
                $pelanggan = Pelanggan::find($hasilAkhir[$i]['pelanggan_id']);
                // $clustering = new HasilClustering;
                // $clustering->pelanggan_id = $pelanggan->id;
                if ($hasilAkhir[$i]['kelompok1'] <= $hasilAkhir[$i]['kelompok2'] && $hasilAkhir[$i]['kelompok1'] <= $hasilAkhir[$i]['kelompok3']) {
                    $hasilUrutan = $hasilUrutan . "1";
                    // $clustering->kelompok = 'kelompok 1';
                    // $clustering->rekomendasi = 35;
                    array_push($hasilFix, ['kesimpulan' => 'kelompok 1', 'pelanggan_id' => $data[$i]['pelanggan_id'], 'nama_pelanggan' => $pelanggan->nama_pelanggan, 'cluster 1' => 35]);
                } else if ($hasilAkhir[$i]['kelompok2'] <= $hasilAkhir[$i]['kelompok1'] && $hasilAkhir[$i]['kelompok2'] <= $hasilAkhir[$i]['kelompok3']) {
                    $hasilUrutan = $hasilUrutan . "2";
                    // $clustering->kelompok = 'kelompok 2';
                    // $clustering->rekomendasi = 20;
                    array_push($hasilFix, ['kesimpulan' => 'kelompok 2', 'pelanggan_id' => $data[$i]['pelanggan_id'], 'nama_pelanggan' => $pelanggan->nama_pelanggan, 'cluster 2' => 20]);
                } else if ($hasilAkhir[$i]['kelompok3'] <= $hasilAkhir[$i]['kelompok1'] && $hasilAkhir[$i]['kelompok3'] <= $hasilAkhir[$i]['kelompok2']) {
                    $hasilUrutan = $hasilUrutan . "3";
                    // $clustering->kelompok = 'kelompok 3';
                    // $clustering->rekomendasi = 5;
                    array_push($hasilFix, ['kesimpulan' => 'kelompok 3', 'pelanggan_id' => $data[$i]['pelanggan_id'], 'nama_pelanggan' => $pelanggan->nama_pelanggan, 'cluster 3' => 5]);
                }
                // $clustering->save();
            }
            $test = strcmp($urutan, $hasilUrutan);

            if ($test == 0) {
                unset($hasilFix); // $hasilFix is gone
                $hasilFix = [];
                for ($i=0; $i < count($hasilAkhir); $i++) { 
                    $pelanggan = Pelanggan::find($hasilAkhir[$i]['pelanggan_id']);
                    $clustering = new HasilClustering;
                    $clustering->pelanggan_id = $pelanggan->id;
                    if ($hasilAkhir[$i]['kelompok1'] <= $hasilAkhir[$i]['kelompok2'] && $hasilAkhir[$i]['kelompok1'] <= $hasilAkhir[$i]['kelompok3']) {
                        $hasilUrutan = $hasilUrutan . "1";
                        $clustering->kelompok = 'kelompok 1';
                        $clustering->rekomendasi = 35;
                        array_push($hasilFix, ['kesimpulan' => 'kelompok 1', 'pelanggan_id' => $data[$i]['pelanggan_id'], 'nama_pelanggan' => $pelanggan->nama_pelanggan, 'cluster 1' => 35]);
                    } else if ($hasilAkhir[$i]['kelompok2'] <= $hasilAkhir[$i]['kelompok1'] && $hasilAkhir[$i]['kelompok2'] <= $hasilAkhir[$i]['kelompok3']) {
                        $hasilUrutan = $hasilUrutan . "2";
                        $clustering->kelompok = 'kelompok 2';
                        $clustering->rekomendasi = 20;
                        array_push($hasilFix, ['kesimpulan' => 'kelompok 2', 'pelanggan_id' => $data[$i]['pelanggan_id'], 'nama_pelanggan' => $pelanggan->nama_pelanggan, 'cluster 2' => 20]);
                    } else if ($hasilAkhir[$i]['kelompok3'] <= $hasilAkhir[$i]['kelompok1'] && $hasilAkhir[$i]['kelompok3'] <= $hasilAkhir[$i]['kelompok2']) {
                        $hasilUrutan = $hasilUrutan . "3";
                        $clustering->kelompok = 'kelompok 3';
                        $clustering->rekomendasi = 5;
                        array_push($hasilFix, ['kesimpulan' => 'kelompok 3', 'pelanggan_id' => $data[$i]['pelanggan_id'], 'nama_pelanggan' => $pelanggan->nama_pelanggan, 'cluster 3' => 5]);
                    }
                    $clustering->save();
                }
            }
        };


        return response()->json([
            'urutan' => $urutan,
            'hasilUrutan' => $hasilUrutan,
            'chimp' => $test
        ]);
    }
}
