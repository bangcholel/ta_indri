<!DOCTYPE html>
<html lang="en">
  <head>
    

    <?php 
    include '../header.php';
    include '../koneksi_db.php';
    ?>
      <!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
              
              	  <p class="centered"><a href="profile.html"><img src="../assets/img/ui-sam.jpg" class="img-circle" width="60"></a></p>
              	  <h5 class="centered">Sales</h5>
              	  	                                 
                  <li class="sub-menu">
                      <a class="active" href="javascript:;" >
                          <i class="fa fa-th"></i>
                          <span>Sales</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="sls_Data_Pesanan.html">Data Pesanan</a></li>
                          <li class="active"><a  href="sls_Input_Pesanan.html">Input Data Pesanan</a></li>
                          <li><a  href="sls_clustering.html">Clustering</a></li>
                          <li><a  href="responsive_table.html">Bantuan</a></li>
                      </ul>
                  </li>
                  
              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
          	<h3><i class="fa fa-angle-right"></i> Input Data Pesanan </h3><br>

              <div class="col-lg-10 " >
                  <div class="form-panel">
                      <form class="form-horizontal style-form" method="get">

                          <div class="form-group ">
                              <label class="col-sm-3 control-label">Id Pesanan</label>
                              <div class="col-sm-2">
                                  <input type="text" class="form-control">
                              </div>
                          </div>

                          <div class="form-group">
                              <label class="col-sm-3 control-label"> Tanggal Pesanan </label>
                              <div class="col-sm-3">
                                  <input type="text" class="form-control" id="disabledInput" disabled="">
                              </div>
                          </div>

        <form class="form-input" method="post" action="trans_sem.php" style="padding-top: 30px;">
          <label>Pilih Barang : </label>
          <select style="width: 372px;cursor: pointer;" required="required" name="ID_BARANG">
            <?php
            $tampil = "SELECT * FROM data_barang ";
            $sql = mysql_query($tampil);
            while($data = mysql_fetch_assoc($sql)) {

              echo "<option value='$data[ID_BARANG]'>$data[NAMA_BARANG] $data[TYPE_BARANG] | Harga : ".number_format($data['HARGA_SATUAN']).")</option>";
            }
            ?>
          </select>
          <label>Jumlah Beli :</label>
          <input required="required" type="number" name="jumlah">
          <input type="hidden" name="trx" value="<?php echo date("d")."/AF/".$_SESSION['id']."/".date("y") ?>">
          <button class="btnblue" type="submit"><i class="fa fa-save"></i> Simpan</button>
        </form>
                          
                          <div class="form-group ">
                              <div class="col-sm-6 ">
                                  <button type="button" class="btn btn-theme pull-right" ><i class="fa fa-save"></i> Save </button>
                              </div>
                              <div class="col-sm-2 ">
                                  <button type="button" class="btn btn-theme pull-right" ><i class="fa fa-eraser"></i> Reset </button>
                              </div>
                              
                          </div>

                      </form>
                  </div>
              </div><!-- col-lg-12-->       
                


		</section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->
  </section>
    <?php 
    include '../footer.php';
    ?>

  </body>
</html>