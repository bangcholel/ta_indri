<!DOCTYPE html>
<html lang="en">
  <head>
    <?php 
    include 'koneksi_db.php';
    ?>


    <title>PT. Andhipaputra Rekayasanusa</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        
    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">


  </head>

  <body>

      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->

	  <div id="login-page">
	  	<div class="container">
	  	
		      <form class="form-login" method='post' action="pro_login.php">
		        <h2 class="form-login-heading">login</h2>
		        <div class="login-wrap">
		            <input type="text" class="form-control" name="USERNAME" placeholder="User ID" autofocus>
		            <br>
		            <input type="password" class="form-control" name="PASSWORD" placeholder="Password">
		            <br><br><br>

		            <button class="btn btn-theme btn-block" type="submit" value="Login" ><i class="fa fa-lock"></i> SIGN IN</button>
		            
		        </div>
		
		      </form>	  	
	  	
	  	</div>
	  </div>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>

    <!--BACKSTRETCH-->
    <!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
    <script type="text/javascript" src="assets/js/jquery.backstretch.min.js"></script>
    <script>
        $.backstretch("assets/img/login-bg.jpg", {speed: 500});
    </script>


  </body>
</html>
