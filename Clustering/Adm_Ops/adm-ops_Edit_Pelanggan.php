<!DOCTYPE html>
<html lang="en">
  <head>
    
    <?php 
    include '../header.php';
    include '../koneksi_db.php';
    ?>
      <!--sidebar start-->

      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
              
              	  <p class="centered"><a href="profile.html"><img src="../assets/img/ui-sam.jpg" class="img-circle" width="60"></a></p>
              	  <h5 class="centered">Admin Operasional</h5>
              	  	                                 
                  <li class="sub-menu">
                      <a class="active" href="javascript:;" >
                          <i class="fa fa-th"></i>
                          <span>Admin Operasional</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="adm-ops_Data_Pelanggan.php">Data Pelanggan</a></li>
                          <li ><a  href="adm-ops_Input_Pelanggan.php">Input Data Pelanggan</a></li>
                          <li><a  href="responsive_table.php">Bantuan</a></li>
                      </ul>
                  </li>
                  
              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
          	<h3><i class="fa fa-angle-right"></i> Edit Data Pelanggan </h3><br>
        

            <!-- BASIC FORM ELELEMNTS -->
              <div class="col-lg-9 " >
                  <div class="form-panel">
                      <form class="form-horizontal style-form" method="POST" action="proses_edit_Pelanggan.php">

                        <?php
                          $id_plg   = $_GET['ID_PELANGGAN'];
                          $query  = mysql_query("select * from data_pelanggan where ID_PELANGGAN='$id_plg'");
                          if($data   = mysql_fetch_array($query)){
                          $nama_plg = $data['NAMA_PELANGGAN'];
                          $tlp_plg = $data['NO_TLP'];
                          $email_plg = $data['EMAIL_PELANGGAN']; 
                          $alamat_plg = $data['ALAMAT_PELANGGAN'];
                        ?>


                          <div class="form-group ">
                              <label class="col-sm-3 control-label">Id Pelanggan</label>
                              <div class="col-sm-3">
                                  <input type="text" class="form-control" name="ID_PELANGGAN" value="<?php echo $_GET['ID_PELANGGAN']; ?>" readonly="readonly">
                              </div>
                          </div>

                          <div class="form-group">
                              <label class="col-sm-3 control-label">Nama Pelanggan</label>
                              <div class="col-sm-7">
                                  <input type="text" class="form-control" name="NAMA_PELANGGAN" value=" <?php echo $nama_plg ?> " >
                              </div>
                          </div>


                          <div class="form-group">
                              <label class="col-sm-3 control-label">No Tlp / HP </label>
                              <div class="col-sm-7">
                                  <input type="text" class="form-control" name="NO_TLP" value="<?php echo $tlp_plg ?>">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-3 control-label">Email Pelanggan</label>
                              <div class="col-sm-7">
                                  <input type="text" class="form-control" name="EMAIL_PELANGGAN" value="<?php echo $email_plg;?>">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-3 control-label">Alamat Pelanggan</label>
                              <div class="col-sm-9">
                                  <textarea class="form-control" name="ALAMAT_PELANGGAN" > <?php echo $alamat_plg;?></textarea>
                              </div>
                          </div>
                          
                          <div class="form-group ">
                              <div class="col-sm-6 ">
                                  <button type="submit" class="btn btn-theme pull-right" name="Edit"><i class="fa fa-save"></i> Save </button>
                              </div>
                                                            
                          </div>

                      <?php
                                      }
                        ?>
                      </form>
                  </div>
              </div><!-- col-lg-12-->       
                


		</section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->
  </section>

    <?php 
    include '../footer.php';
    ?>

  </body>
</html>