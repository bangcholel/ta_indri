<!DOCTYPE html>
<html lang="en">
  <head>
    
    <?php 
    include '../header.php';
    include '../koneksi_db.php';
    ?>
      <!--sidebar start-->

      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
              
              	  <p class="centered"><a href="profile.html"><img src="../assets/img/ui-sam.jpg" class="img-circle" width="60"></a></p>
              	  <h5 class="centered">Admin Operasional</h5>
              	  	                                 
                  <li class="sub-menu">
                      <a class="active" href="javascript:;" >
                          <i class="fa fa-th"></i>
                          <span>Admin Operasional</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="adm-ops_Data_Pelanggan.php">Data Pelanggan</a></li>
                          <li class="active"><a  href="adm-ops_Input_Pelanggan.php">Input Data Pelanggan</a></li>
                          <li><a  href="responsive_table.php">Bantuan</a></li>
                      </ul>
                  </li>
                  
              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
          	<h3><i class="fa fa-angle-right"></i> Input Data Pelanggan </h3><br>
        

            <!-- BASIC FORM ELELEMNTS -->
              <div class="col-lg-9 " >
                  <div class="form-panel">
                      <form class="form-horizontal style-form" method="GET" action="proses_Input_Pelanggan.php">

                          <div class="form-group ">
                              <label class="col-sm-3 control-label">Id Pelanggan</label>
                              <div class="col-sm-3">
                                  <input type="text" class="form-control" name="ID_PELANGGAN">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-3 control-label">Nama Pelanggan</label>
                              <div class="col-sm-7">
                                  <input type="text" class="form-control" name="NAMA_PELANGGAN">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-3 control-label">No Tlp / HP </label>
                              <div class="col-sm-7">
                                  <input type="text" class="form-control" name=" NO_TLP">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-3 control-label">Email Pelanggan</label>
                              <div class="col-sm-7">
                                  <input type="text" class="form-control" name="EMAIL_PELANGGAN">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-3 control-label">Alamat Pelanggan</label>
                              <div class="col-sm-9">
                                  <textarea class="form-control" name="ALAMAT_PELANGGAN"></textarea>
                              </div>
                          </div>
                          
                          <div class="form-group ">
                              <div class="col-sm-6 ">
                                  <button type="submit" class="btn btn-theme pull-right" name="simpan"><i class="fa fa-save"></i> Save </button>
                              </div>
                              <div class="col-sm-2 ">
                                  <button type="reset" class="btn btn-theme pull-right" ><i class="fa fa-eraser"></i> Reset </button>
                              </div>
                              
                          </div>

                      </form>
                  </div>
              </div><!-- col-lg-12-->       
                


		</section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->
  </section>

    <?php 
    include '../footer.php';
    ?>

  </body>
</html>