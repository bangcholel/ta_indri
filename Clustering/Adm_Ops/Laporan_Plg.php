<?php
	// memanggil library FPDF
	require('../assets/fpdf/fpdf.php');
	include '../koneksi_db.php';
	// intance object dan memberikan pengaturan halaman PDF
	$pdf = new FPDF('L','mm','A4');
	// membuat halaman baru
	$pdf->AddPage();

	// ---- judul ------
	$pdf->SetFont('Arial','B',18); // setting jenis font yang akan digunakan
	$pdf->Cell(0,10, 'LAPORAN DATA PELANGGAN', '0', 1, 'C'); // mencetak string 
	$pdf->SetFont('helvetica','',14); // setting jenis font yang akan digunakan
	$pdf->Cell(0,7, 'PT. Andhipaputra Rekayasanusa ', '0', 1, 'C'); // mencetak string 
	$pdf->SetFont('helvetica','I',12); // setting jenis font yang akan digunakan
	$pdf->Cell(0,7, 'Supply hardware & accesories door and windows', '0', 1, 'C'); // mencetak string 
	$pdf->SetFont('helvetica','I',10);
	$pdf->Cell(0,7, 'Jl. Embong Sawo 2 Kav. 8-9  Surabaya | TLp: 031-5480808 Fax: 031-5481818', '0', 1, 'C'); // mencetak string 

	
	// Memberikan space kebawah agar tidak terlalu rapat
	$pdf->Cell(10,7,'',0,1);
	 
	$pdf->SetFont('Arial','B',10);
	$pdf->Cell(30,6,'ID PELANGGAN',1,0,'C');
	$pdf->Cell(70,6,'NAMA PELANGGAN',1,0,'C');
	$pdf->Cell(40,6,'NO TLP',1,0,'C');
	$pdf->Cell(60,6,'EMAIL PELANGGAN',1,0,'C');
	$pdf->Cell(80,6,'ALAMAT PELANGGAN',1,1,'C');
	 
	$pdf->SetFont('Arial','',10);

	 
	$tampil = "SELECT * FROM data_pelanggan ";
    $sql = mysql_query($tampil);
	while($data = mysql_fetch_array($sql)) {
			$pdf->Cell(30,6,$data['ID_PELANGGAN'],1,0);
			$pdf->Cell(70,6,$data['NAMA_PELANGGAN'],1,0);
			$pdf->Cell(40,6,$data['NO_TLP'],1,0);
			$pdf->Cell(60,6,$data['EMAIL_PELANGGAN'],1,0);
			$pdf->Cell(80,6,$data['ALAMAT_PELANGGAN'],1,1);
	}

	$pdf->Output();


?>