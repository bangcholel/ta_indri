<!DOCTYPE html>
<html lang="en">
  <head>
    
    <?php 
    include '../header.php';
    include '../koneksi_db.php';
    ?>
      <!--sidebar start-->

      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
              
                  <p class="centered"><img src="../assets/img/ui-sam.jpg" class="img-circle" width="60"></a></p>
                  <h5 class="centered">Purchasing</h5>
                                                     
                  <li class="sub-menu">
                      <a class="active" href="javascript:;" >
                          <i class="fa fa-th"></i>
                          <span>Purchasing</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="Purc_Data_Barang.php">Data Barang</a></li>
                          <li><a  href="Purc_Input_Barang.php">Input Data Barang</a></li>
                          <li><a  href="responsive_table.php">Bantuan</a></li>
                      </ul>
                      </ul>
                  </li>
                  
              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
          	<h3><i class="fa fa-angle-right"></i> Edit Data Barang </h3><br>
        

            <!-- BASIC FORM ELELEMNTS -->
              <div class="col-lg-9 " >
                  <div class="form-panel">
                      <form class="form-horizontal style-form" method="POST" action="proses_edit_Barang.php">

                        <?php
                          $id_brg   = $_GET['ID_BARANG'];
                          $query  = mysql_query("select * from data_barang where ID_BARANG='$id_brg'");
                          if($data   = mysql_fetch_array($query)){
                          $nama_brg = $data['NAMA_BARANG'];
                          $type_brg = $data['TYPE_BARANG'];
                          $merk_brg = $data['MERK_BARANG']; 
                          $satuan = $data['SATUAN'];
                          $harga_stuan = $data['HARGA_SATUAN'];
                        ?>

                          <div class="form-group ">
                              <label class="col-sm-3 control-label">Id Barang</label>
                              <div class="col-sm-3">
                                  <input type="text" class="form-control" name="ID_BARANG" value="<?php echo $_GET['ID_BARANG']; ?>" readonly="readonly">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-3 control-label">Nama Barang</label>
                              <div class="col-sm-7">
                                  <input type="text" class="form-control" name="NAMA_BARANG" value= "<?php echo $nama_brg ?> ">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-3 control-label"> Type Barang </label>
                              <div class="col-sm-7">
                                  <input type="text" class="form-control" name="TYPE_BARANG" value= "<?php echo $type_brg ?> ">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-3 control-label">Merk Barang</label>
                              <div class="col-sm-7">
                                  <input type="text" class="form-control" name="MERK_BARANG" value= "<?php echo $merk_brg ?> ">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-3 control-label">Satuan</label>
                              <div class="col-sm-9">
                                  <select class="btn btn-theme dropdown-toggle" name="SATUAN">
                                    <option value="" <?php if( $satuan==''){echo "selected"; } ?> > - Satuan -</option>
                                    <option value="BUAH" <?php if( $satuan=='BUAH'){echo "selected"; } ?>>Buah</option>
                                    <option value="PASANG" <?php if( $satuan=='PASANG'){echo "selected"; } ?>>Pasang</option>
                                  </select>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-3 control-label">Harga</label>
                              <div class="col-sm-3">
                                  <input type="text" class="form-control" name="HARGA_SATUAN" value= "<?php echo $harga_stuan ?> ">
                              </div>
                          </div>
                          
                          <div class="form-group ">
                              <div class="col-sm-6 ">
                                  <button type="submit" class="btn btn-theme pull-right" name="Edit"><i class="fa fa-save"></i> Save </button>
                              </div>
                                                            
                          </div>

                      <?php
                                      }
                        ?>
                      </form>
                  </div>
              </div><!-- col-lg-12-->       
                


		</section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->
  </section>

    <?php 
    include '../footer.php';
    ?>

  </body>
</html>