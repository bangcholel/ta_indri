<!DOCTYPE html>
<html lang="en">
  <head>
    
    <?php 
    include '../header.php';
    include '../koneksi_db.php';
    ?>
      <!--sidebar start-->

      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
              
                  <p class="centered"><img src="../assets/img/ui-sam.jpg" class="img-circle" width="60"></a></p>
                  <h5 class="centered">Purchasing</h5>
                                                     
                  <li class="sub-menu">
                      <a class="active" href="javascript:;" >
                          <i class="fa fa-th"></i>
                          <span>Purchasing</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="Purc_Data_Barang.php">Data Barang</a></li>
                          <li class="active"><a  href="Purc_Input_Barang.php">Input Data Barang</a></li>
                          <li><a  href="responsive_table.php">Bantuan</a></li>
                      </ul>
                      </ul>
                  </li>
                  
              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
          	<h3><i class="fa fa-angle-right"></i> Input Data Barang </h3><br>
        

            <!-- BASIC FORM ELELEMNTS -->
              <div class="col-lg-9 " >
                  <div class="form-panel">
                      <form class="form-horizontal style-form" method="GET" action="proses_Input_Barang.php">

                          <div class="form-group ">
                              <label class="col-sm-3 control-label">Id Barang</label>
                              <div class="col-sm-3">
                                  <input type="text" class="form-control" name="ID_BARANG">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-3 control-label">Nama Barang</label>
                              <div class="col-sm-7">
                                  <input type="text" class="form-control" name="NAMA_BARANG">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-3 control-label"> Type Barang </label>
                              <div class="col-sm-7">
                                  <input type="text" class="form-control" name="TYPE_BARANG">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-3 control-label">Merk Barang</label>
                              <div class="col-sm-7">
                                  <input type="text" class="form-control" name="MERK_BARANG">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-3 control-label">Satuan</label>
                              <div class="col-sm-9">
                                  <select class="btn btn-theme dropdown-toggle" name="SATUAN">
                                    <option value="" >- Satuan -</option>
                                    <option value="BUAH">Buah</option>
                                    <option value="PASANG">Pasang</option>
                                  </select>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-3 control-label">Harga</label>
                              <div class="col-sm-3">
                                  <input type="text" class="form-control" name="HARGA_SATUAN">
                              </div>
                          </div>
                          
                          <div class="form-group ">
                              <div class="col-sm-6 ">
                                  <button type="submit" class="btn btn-theme pull-right" name="simpan"><i class="fa fa-save"></i> Save </button>
                              </div>
                              <div class="col-sm-2 ">
                                  <button type="reset" class="btn btn-theme pull-right" ><i class="fa fa-eraser"></i> Reset </button>
                              </div>
                              
                          </div>

                      </form>
                  </div>
              </div><!-- col-lg-12-->       
                


		</section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->
  </section>

    <?php 
    include '../footer.php';
    ?>

  </body>
</html>