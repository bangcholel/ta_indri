<?php
	// memanggil library FPDF
	require('../assets/fpdf/fpdf.php');
	include '../koneksi_db.php';
	// intance object dan memberikan pengaturan halaman PDF
	$pdf = new FPDF('L','mm','A4');
	// membuat halaman baru
	$pdf->AddPage();

	// ---- judul ------
	$pdf->SetFont('Arial','B',18); // setting jenis font yang akan digunakan
	$pdf->Cell(0,10, 'LAPORAN DATA BARANG', '0', 1, 'C'); // mencetak string 
	$pdf->SetFont('helvetica','',14); // setting jenis font yang akan digunakan
	$pdf->Cell(0,7, 'PT. Andhipaputra Rekayasanusa ', '0', 1, 'C'); // mencetak string 
	$pdf->SetFont('helvetica','I',12); // setting jenis font yang akan digunakan
	$pdf->Cell(0,7, 'Supply hardware & accesories door and windows', '0', 1, 'C'); // mencetak string 
	$pdf->SetFont('helvetica','I',10);
	$pdf->Cell(0,7, 'Jl. Embong Sawo 2 Kav. 8-9  Surabaya | TLp: 031-5480808 Fax: 031-5481818', '0', 1, 'C'); // mencetak string 

	
	// Memberikan space kebawah agar tidak terlalu rapat
	$pdf->Cell(40,7,'',0,1);
	 
	$pdf->SetFont('Arial','B',10);
	$pdf->Cell(10,6,'NO',1,0,'C');
	$pdf->Cell(30,6,'ID BARANG',1,0,'C');
	$pdf->Cell(50,6,'NAMA BARANG',1,0,'C');
	$pdf->Cell(70,6,'TYPE BARANG',1,0,'C');
	$pdf->Cell(30,6,'MERK BARANG',1,0,'C');
	$pdf->Cell(20,6,'SATUAN',1,0,'C');
	$pdf->Cell(40,6,'HARGA SATUAN',1,1,'C');
	 
	$pdf->SetFont('Arial','',10);

	 
	$tampil = "SELECT * FROM DATA_BARANG ";
    $sql = mysql_query($tampil);
    $No=1;
	while($data = mysql_fetch_array($sql)) {
			$pdf->Cell(10,6,$No,1,0);
			$pdf->Cell(30,6,$data['ID_BARANG'],1,0);
			$pdf->Cell(50,6,$data['NAMA_BARANG'],1,0);
			$pdf->Cell(70,6,$data['TYPE_BARANG'],1,0);
			$pdf->Cell(30,6,$data['MERK_BARANG'],1,0);
			$pdf->Cell(20,6,$data['SATUAN'],1,0);
			$pdf->Cell(40,6,$data['HARGA_SATUAN'],1,1);
			$No++;
	}

	$pdf->Output();


?>