<!DOCTYPE html>
<html lang="en">
  <head>
    
    <?php 
    include '../header.php';
    include '../koneksi_db.php';

    session_start();
    if(!isset($_SESSION['username']) && !isset($_SESSION['password'])){
        //jika session belum di set/register
        echo "<a href='login-aprn.php'>Login Form</a><br>";
    die("Anda belum Login Silahkan Login Terlebihdahulu");
        
    }

    ?>
     <!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
              
                  <p class="centered"><img src="../assets/img/ui-sam.jpg" class="img-circle" width="60"></a></p>
                  <h5 class="centered">Purchasing</h5>
                                                     
                  <li class="sub-menu">
                      <a class="active" href="javascript:;" >
                          <i class="fa fa-th"></i>
                          <span>Purchasing</span>
                      </a>
                      <ul class="sub">
                          <li class="active"><a  href="Purc_Data_Barang.php">Data Barang</a></li>
                          <li><a  href="Purc_Input_Barang.php">Input Data Barang</a></li>
                          <li><a  href="responsive_table.php">Bantuan</a></li>
                      </ul>
                  </li>
                  
              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->


      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
          	<h3><i class="fa fa-angle-right"></i> Data Barang </h3><br><br>
        

              <form class="form-inline" >       
                <input type="text" class="form-control round-form"  placeholder="Search" name="cari">
                <button class="btn btn-primary btn-xs" name="submit" id="submit"><i class="fa fa-search"></i></button>

                <a href="Laporan_Brg.php">
                  <button type="button" class="btn btn-theme pull-right" ><i class="fa fa-print"></i> Print </button>
                </a>
              </form>

              <div class="row mt">
                  <div class="col-md-12">
                      <div class="content-panel">
                          <table class="table table-striped table-advance table-hover">
	                  	  	   
                              <thead>
                              <tr>
                                  <th></i>No</th>
                                  <th class="hidden-phone"> Id Barang </th>
                                  <th>Nama Barang </th>
                                  <th>Type Barang </th>
                                  <th>Merk </th>
                                  <th>Satuan </th>
                                  <th>Harga Satuan </th>
                                  <th>Action </th>
                              </tr>
                              </thead>
                              <?php
                                if(isset($_GET['cari'])){
                                  $cari = $_GET['cari'];
                                  $tampil = "SELECT * FROM data_barang where NAMA_BARANG like '%".$cari."%'";
                                  $sql = mysql_query($tampil);
                                }else{
                                  $tampil = "SELECT * FROM data_barang ";
                                  $sql = mysql_query($tampil);
                                }
                                $no = 1;
                                while($data = mysql_fetch_array($sql)) {
                              ?>
                              <tbody>
                              <tr>
                                  <td><?php echo $no ?></td>
                                  <td ><?php echo $data["ID_BARANG"];?></td>
                                  <td><?php echo $data["NAMA_BARANG"];?></td>
                                  <td><?php echo $data["TYPE_BARANG"];?> </td>
                                  <td><?php echo $data["MERK_BARANG"];?> </td>
                                  <td><?php echo $data["SATUAN"];?> </td>
                                  <td><?php echo $data["HARGA_SATUAN"];?> </td>
                                  <td>
                                      <a href='Purc_Edit_Barang.php?ID_BARANG=<?php echo $data["ID_BARANG"] ; ?>'> 
                                        <button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button>
                                      </a>
                                      <a href="proses_Hapus_Barang.php?ID_BARANG=<?php echo $data["ID_BARANG"] ; ?>" >
                                        <button class="btn btn-danger btn-xs" ><i class="fa fa-trash-o"></i></button>
                                      </a>
                                  </td>
                              </tr>
                              
                              </tbody>
                          <?php $no++;   
                           } ?>


                          </table>

                      </div><!-- /content-panel -->
                  </div><!-- /col-md-12 -->
              </div><!-- /row -->

            	<form class="form-inline" onSubmit="return validateForm()" action="Purc_Data_Barang.php" method="post" enctype="multipart/form-data"> <br>	
              	<a href="Format-Brg.xls"> <button type="button" class="btn btn-theme " ><i class="fa fa-download"></i> Format Upload </button></a>
              	<br><br>	
              	<input type="file" id="FileDatabrg" class="form-control " name="FileDatabrg" required />
              	<button type="submit" class="btn btn-theme " name="submit"><i class="fa fa-upload"></i> Upload File </button>
            	</form>

              <?php

                //memanggil file excel_reader
                require "../excel_reader.php";
                 
                //jika tombol import ditekan
                if(isset($_POST['submit'])){
                    // file yang tadinya di upload, di simpan di temporary file PHP, file tersebut yang kita ambil
                    // dan baca dengan PHP Excel Class
                    $target = basename($_FILES['FileDatabrg']['name']) ;
                    move_uploaded_file($_FILES['FileDatabrg']['tmp_name'], $target);
                    
                    $data = new Spreadsheet_Excel_Reader($_FILES['FileDatabrg']['name'],false);
                    
                //    menghitung jumlah baris file xls
                    $baris = $data->rowcount($sheet_index=0);
                    
                    
                //    import data excel mulai baris ke-2 (karena tabel xls ada header pada baris 1)
                    for ($i=2; $i<=$baris; $i++)
                    {
                //       membaca data (kolom ke-1 sd terakhir)
                      $ID_BARANG       = $data->val($i, 1);
                      $NAMA_BARANG     = $data->val($i, 2);
                      $TYPE_BARANG     = $data->val($i, 3);
                      $MERK_BARANG     = $data->val($i, 4);
                      $SATUAN          = $data->val($i, 5);
                      $HARGA_SATUAN    = $data->val($i, 6);
      
                 
                //      setelah data dibaca, masukkan ke tabel pegawai sql
                      $query = "INSERT INTO data_barang VALUES 
                                ('$ID_BARANG','$NAMA_BARANG' ,'$TYPE_BARANG', '$MERK_BARANG', '$SATUAN', '$HARGA_SATUAN')";
                      $hasil = mysql_query($query);
                    }
                    
                    if(!$hasil){
                //          jika import gagal
                          die(mysql_error());
                          echo "<script>alert ('upload gagal '); </script> ";
                      }else{
                //          jika impor berhasil
                          echo "<script>alert ('upload berhasil ');document.location='Purc_Data_Barang.php' </script> ";
                    }
                    
                //    hapus file xls yang udah dibaca
                    unlink($_FILES['FileDatabrg']['name']);
                }
                 
                ?>

              <script type="text/javascript">
               //    validasi form (hanya file .xls yang diijinkan)
               function validateForm()
                {
                  function hasExtension(inputID, exts) {
                  var fileName = document.getElementById(inputID).value;
                  return (new RegExp('(' + exts.join('|').replace(/\./g, '\\.') + ')$')).test(fileName);
                  }
       
                if(!hasExtension('FileDatabrg', ['.xls'])){
                  alert("Hanya file XLS (Excel 2003) yang diijinkan.");
                  return false;
                  }
                }
              </script>


                

		</section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->
  </section>
    <?php 
    include '../footer.php';
    ?>


  </body>
</html>
