<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/test', 'TestController@test');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::group(['middleware' => ['auth']], function() {
    Route::resource('barang', 'BarangController');
    Route::resource('pelanggan', 'PelangganController');
    Route::resource('transaksi', 'TransaksiController');
    Route::put('transaksi/{id}/fix', 'TransaksiController@fix')->name('transaksi.fix');

    Route::resource('transformasi', 'TransformasiController');
});
