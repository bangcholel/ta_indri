@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-primary">
                <div class="panel-heading">Tambah Pelanggan</div>

                <div class="panel-body">
                    <form action="{{ route('pelanggan.update', $pelanggan->id) }}" method="POST">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <div class="form-group">
                            <div class="col-md-3"><label>NAMA PELANGGAN</label></div>
                            <div class="col-md-9"><input type="text" name="nama_pelanggan" value="{{ $pelanggan->nama_pelanggan }}" class="form-control"></div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3"><label>NO TELPON</label></div>
                            <div class="col-md-9"><input type="text" name="no_telp" value="{{ $pelanggan->no_telp }}" class="form-control"></div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3"><label>EMAIL</label></div>
                            <div class="col-md-9"><input type="text" name="email" value="{{ $pelanggan->email }}" class="form-control"></div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3"><label>ALAMAT</label></div>
                            <div class="col-md-9">
                                <textarea name="alamat" id="alamat" cols="30" rows="10" class="form-control">{{ $pelanggan->alamat }}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-9 col-md-offset-3"><button type="submit" class="btn btn-block btn-success">PERBARUI</button></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
