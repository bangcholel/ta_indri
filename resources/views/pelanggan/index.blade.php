@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-primary">
                <div class="panel-heading">Data Pelanggan</div>

                <div class="panel-body">
                    <table class="table">
                        <thead>
                            <th>ID PELANGGAN</th>
                            <th>NAMA PELANGGAN</th>
                            <th>NO TELP</th>
                            <th>EMAIL</th>
                            <th>ALAMAT</th>
                            <th>ACTION</th>
                        </thead>
                        <tbody>
                            @foreach($pelanggans as $pelanggan)
                                <tr>
                                    <td>{{ $pelanggan->id }}</td>
                                    <td>{{ $pelanggan->nama_pelanggan }}</td>
                                    <td>{{ $pelanggan->no_telp }}</td>
                                    <td>{{ $pelanggan->email }}</td>
                                    <td>{{ $pelanggan->alamat }}</td>
                                    <td>
                                        <a href="{{ route('pelanggan.edit', $pelanggan->id) }}" class="btn btn-info">edit</a>
                                        <button type="button" class="btn btn-danger" data-toggle="modal"
                                            data-target="#modalDeleteHapusPelanggan{{ $pelanggan->id }}">
                                            Hapus
                                        </button>

                                        <div class="modal fade" id="modalDeleteHapusPelanggan{{ $pelanggan->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header text-center">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title" id="myModalLabel">Anda yakin ingin menghapus ?</h4>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <form action="{{ route('pelanggan.destroy', $pelanggan->id) }}" method="post">
                                                            {{ csrf_field() }}
                                                            {{ method_field('DELETE') }}
                                                            <button type="button" class="btn btn-danger" data-dismiss="modal">BATAL</button>
                                                            <button type="submit" class="btn btn-success">HAPUS</button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
