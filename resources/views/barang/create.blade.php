@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-primary">
                <div class="panel-heading">Tambah Barang</div>

                <div class="panel-body">
                    <form action="{{ route('barang.store') }}" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <div class="col-md-3"><label>NAMA BARANG</label></div>
                            <div class="col-md-9"><input type="text" name="nama_barang" class="form-control"></div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3"><label>TYPE BARANG</label></div>
                            <div class="col-md-9"><input type="text" name="type_barang" class="form-control"></div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3"><label>MERK BARANG</label></div>
                            <div class="col-md-9"><input type="text" name="merek_barang" class="form-control"></div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3"><label>SATUAN</label></div>
                            <div class="col-md-9">
                                <select name="satuan" id="satuan" class="form-control">
                                    <option value="">SATUAN...</option>
                                    <option value="BUAH">BUAH</option>
                                    <option value="PASANG">PASANG</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3"><label>HARGA</label></div>
                            <div class="col-md-9"><input name="harga_satuan" type="text" class="form-control"></div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-9 col-md-offset-3"><button type="submit" class="btn btn-block btn-success">SIMPAN</button></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
