@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-primary">
                <div class="panel-heading">Data Barang</div>

                <div class="panel-body">
                    <table class="table">
                        <thead>
                            <th>ID BARANG</th>
                            <th>NAMA BARANG</th>
                            <th>TYPE BARANG</th>
                            <th>MERK</th>
                            <th>SATUAN</th>
                            <th>HARGA</th>
                            <th>ACTION</th>
                        </thead>
                        <tbody>
                            @foreach($barangs as $barang)
                                <tr>
                                    <td>{{ $barang->id }}</td>
                                    <td>{{ $barang->nama_barang }}</td>
                                    <td>{{ $barang->type_barang }}</td>
                                    <td>{{ $barang->merek_barang }}</td>
                                    <td>{{ $barang->satuan }}</td>
                                    <td>{{ $barang->harga_satuan }}</td>
                                    <td>
                                        <a href="{{ route('barang.edit', $barang->id) }}" class="btn btn-info">edit</a>
                                        <button type="button" class="btn btn-danger" data-toggle="modal"
                                            data-target="#modalDeleteHapusBarang{{ $barang->id }}">
                                            Hapus
                                        </button>

                                        <div class="modal fade" id="modalDeleteHapusBarang{{ $barang->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header text-center">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title" id="myModalLabel">Anda yakin ingin menghapus ?</h4>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <form action="{{ route('barang.destroy', $barang->id) }}" method="post">
                                                            {{ csrf_field() }}
                                                            {{ method_field('DELETE') }}
                                                            <button type="button" class="btn btn-danger" data-dismiss="modal">BATAL</button>
                                                            <button type="submit" class="btn btn-success">HAPUS</button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
