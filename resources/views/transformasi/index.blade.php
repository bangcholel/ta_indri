@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <button type="button" class="btn btn-success" data-toggle="modal"
                data-target="#modalCreateTransaksi" style="margin-bottom: 10px;">
                Buat Transformasi
            </button>

            <div class="modal fade" id="modalCreateTransaksi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header text-center">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Buat Transformasi Baru</h4>
                        </div>
                        <div class="modal-footer">
                            <form action="{{ route('transformasi.store') }}" method="post">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <select name="periode_clustering" id="periode_clustering" class="form-control">
                                        <option value="">Pilih Bulan</option>
                                        <option value="01">Januari</option>
                                        <option value="02">Pebruari</option>
                                        <option value="03">Maret</option>
                                        <option value="04">April</option>
                                        <option value="05">Mei</option>
                                        <option value="06">Juni</option>
                                        <option value="07">Juli</option>
                                        <option value="08">Agustus</option>
                                        <option value="09">September</option>
                                        <option value="10">Oktober</option>
                                        <option value="11">November</option>
                                        <option value="12">Desember</option>
                                    </select>
                                </div>
                                <button type="button" class="btn btn-danger" data-dismiss="modal">BATAL</button>
                                <button type="submit" class="btn btn-success">SIMPAN</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-primary">
                <div class="panel-heading">Data Transaksi Pemesanan</div>

                <div class="panel-body">
                    <table class="table">
                        <thead>
                            <th>PERIODE CLUSTERING</th>
                            <th>PELANGGAN</th>
                            <th>TRANS TOTAL</th>
                            <th>TRANS FREKUENSI</th>
                            <th>TRANS BARANG</th>
                        </thead>
                        <tbody>
                            @foreach($transformasi as $t)
                                <tr>
                                    <td>{{ $t->periode_clustering }}</td>
                                    <td>{{ $t->pelanggan->nama_pelanggan }}</td>
                                    <td>{{ $t->trans_total }}</td>
                                    <td>{{ $t->trans_frekuensi }}</td>
                                    <td>{{ $t->trans_barang }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
