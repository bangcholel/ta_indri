@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <button type="button" class="btn btn-success" data-toggle="modal"
                data-target="#modalCreateTransaksi" style="margin-bottom: 10px;">
                Buat Transasksi
            </button>

            <div class="modal fade" id="modalCreateTransaksi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header text-center">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Buat Transaksi Baru</h4>
                        </div>
                        <div class="modal-footer">
                            <form action="{{ route('transaksi.store') }}" method="post">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <select name="pelanggan_id" id="pelanggan_id" class="form-control">
                                        <option value="">Pilih Pelanggan</option>
                                        @foreach($pelanggan as $p)
                                            <option value="{{ $p->id }}">{{ $p->nama_pelanggan }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <button type="button" class="btn btn-danger" data-dismiss="modal">BATAL</button>
                                <button type="submit" class="btn btn-success">SIMPAN</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-primary">
                <div class="panel-heading">Data Transaksi Pemesanan</div>

                <div class="panel-body">
                    <table class="table">
                        <thead>
                            <th>ID</th>
                            <th>PELANGGAN</th>
                            <th>PESAN TANGGAL</th>
                            <th>ACTION</th>
                        </thead>
                        <tbody>
                            @foreach($transaksi as $t)
                                <tr>
                                    <td>{{ $t->id }}</td>
                                    <td>{{ $t->pelanggan->nama_pelanggan }}</td>
                                    <td>{{ date('d-m-Y', strtotime($t->created_at)) }}</td>
                                    <td>
                                        <a href="{{ route('transaksi.show', $t->id) }}" class="btn btn-info">Tambah Detil</a>
                                        <button type="button" class="btn btn-danger" data-toggle="modal"
                                            data-target="#modalDeleteHapusTransaksi{{ $t->id }}">
                                            Hapus
                                        </button>

                                        <div class="modal fade" id="modalDeleteHapusTransaksi{{ $t->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header text-center">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title" id="myModalLabel">Anda yakin ingin menghapus ?</h4>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <form action="{{ route('transaksi.destroy', $t->id) }}" method="post">
                                                            {{ csrf_field() }}
                                                            {{ method_field('DELETE') }}
                                                            <button type="button" class="btn btn-danger" data-dismiss="modal">BATAL</button>
                                                            <button type="submit" class="btn btn-success">HAPUS</button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
