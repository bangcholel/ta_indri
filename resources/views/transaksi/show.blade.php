@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-primary">
                <div class="panel-heading">Detil Transaksi {{ $transaksi->pelanggan->nama_pelanggan }}</div>

                <div class="panel-body">
                    @if($transaksi->status == 0)
                        <div class="col-md-4">
                            @foreach ($errors->all() as $error)
                                {{ $error }}<br/>
                            @endforeach
                            <form action="#" method="POST">
                                {{ csrf_field() }}
                                {{ method_field('PUT') }}
                                <div class="form-group">
                                    <label>NAMA BARANG :</label>
                                    <select name="barang_id" id="barang_id" class="form-control">
                                        <option value="">Pilih Barang</option>
                                        @foreach($barang as $b)
                                            <option value="{{ $b->id }}">{{ $b->nama_barang }} | Harga : {{ $b->harga_satuan }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>JUMLAH BELI :</label>
                                    <input type="text" class="form-control" name="jumlah_beli">
                                </div>
                                <div class="form-group">
                                    <div class="col-md-9 col-md-offset-3"><button type="submit" class="btn btn-block btn-success">TAMBAH</button></div>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-8">
                            <table class="table">
                                <thead>
                                    <th>NAMA BARANG</th>
                                    <th>JUMLAH BELI</th>
                                    <th>HARGA SATUAn</th>
                                    <th>TOTAL</th>
                                </thead>
                                <tbody>
                                    @foreach($detil as $d)
                                        <tr>
                                            <td>{{ $d->barang->nama_barang }}</td>
                                            <td>{{ $d->jumlah_beli }}</td>
                                            <td>{{ $d->barang->harga_satuan }}</td>
                                            <td>{{ $d->total }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <table class="table">
                                <thead>
                                    <?php $diskon = $transaksi->total * $transaksi->diskon / 100; ?>
                                    <th>Total : @if($transaksi->total == null) 0 @else {{ $transaksi->total }} @endif</th>
                                    <th>Diskon : {{ $transaksi->diskon }} %</th>
                                    <th>Grand Total : {{ $transaksi->total - $diskon }}</th>
                                    <th>&nbsp;</th>
                                </thead>
                            </table>
                            <form action="{{ route('transaksi.fix', $transaksi->id) }}" method="POST">
                                {{ csrf_field() }}
                                {{ method_field('PUT') }}
                                <input type="hidden" name="total" value="{{ $transaksi->total }}">
                                <button class="btn btn-success" type="submit">TRANSAKSI DISIMPAN</button>
                            </form>
                        </div>
                    @else
                        <div class="col-md-12">
                            <table class="table">
                                <thead>
                                    <th>NAMA BARANG</th>
                                    <th>JUMLAH BELI</th>
                                    <th>HARGA SATUAn</th>
                                    <th>TOTAL</th>
                                </thead>
                                <tbody>
                                    @foreach($detil as $d)
                                        <tr>
                                            <td>{{ $d->barang->nama_barang }}</td>
                                            <td>{{ $d->jumlah_beli }}</td>
                                            <td>{{ $d->barang->harga_satuan }}</td>
                                            <td>{{ $d->total }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <table class="table">
                                <thead>
                                    <?php $diskon = $transaksi->total * $transaksi->diskon / 100; ?>
                                    <th>Total : @if($transaksi->total == null) 0 @else {{ $transaksi->total }} @endif</th>
                                    <th>Diskon : {{ $transaksi->diskon }} %</th>
                                    <th>Grand Total : {{ $transaksi->total - $diskon }}</th>
                                    <th>&nbsp;</th>
                                </thead>
                            </table>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
